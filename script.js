// 2. Link the script.js file to the index.html file.
console.log("Hello World");
//3. Create a variable getCube and use the exponent operator to compute the cube of a number
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.
//6. Destructure the array and print out a message with the full address using Template Literals.
const myAddress = ["258", "Washington Ave NW","California","90011"];
const [street, avenue, state, zipCode] = myAddress;
console.log(`I live at ${street} ${avenue}, ${state}, ${zipCode}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
//8. Destructure the object and print out a message with the details of the animal using Template Literals.
const animal = {
	name: "Lolong",
	specie: "saltwater crocodile",
	weight: 1075,
	height: "2 ft 3 in"
}

const {name,specie,weight,height} = animal;
console.log(`${name} was a ${specie}. He weight at ${weight} with a measurement of ${height}. `);
//9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

const number = [1,2,3,4,5];
number.forEach((number) => console.log(`${number}`));
//11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const mynumber = number.reduce((x,y) => {return x+y});
console.log(mynumber);

class Dog{
	constructor ( name, age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	} 
}

const myDog = new Dog();
myDog.name = "Doggy"
myDog.age = 5;
myDog.breed = "Askal";
console.log(myDog);





